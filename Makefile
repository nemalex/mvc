CFLAGS = -pedantic -W -Wall
PREFIX = /usr/local

mvc: mvc.c
	$(CC) $(CFLAGS) $? -o $@

install: mvc
	mkdir -p ${PREFIX}/bin
	cp -f mvc ${PREFIX}/bin
	chmod 755 ${PREFIX}/bin/mvc

uninstall:
	rm -f ${PREFIX}/bin/mvc

clean:
	rm -f mvc

.PHONY: clean
