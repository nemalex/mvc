#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

int
ok(char c) {
	int b[] = {32, 33, 34, 39 ,40, 41, 91, 93};
	size_t n = sizeof(b)/sizeof(b[0]);
	for (size_t i = 0; i < n; ++i) {
		if (c == b[i]) return 0;
	}
	return 1;
}

/* TODO
 * adding dry run option and therefore option handling
 */

int
main(int argc, char **argv) {
	if (argc != 2) {
		fprintf(stderr, "Supply only one argument!\n");
		return 1;
	}
	char *s = argv[1];
	if (access(s, F_OK ) != 0) {
		fprintf(stderr, "Input file does not exists!\n");
		return 1;
	} 
	int i = 0;
	int c = 0;
	char buf[256] = "";
	while (s[i] != '\0') {
		if (s[i] >= 65 && s[i] <= 90) {
			sprintf(buf + strlen(buf), "%c", s[i] + 32);
			++c;
		} else if (!ok(s[i])) {
			sprintf(buf + strlen(buf), "_");
			++c;
		} else {
			sprintf(buf + strlen(buf), "%c", s[i]);
		}
		if (i >= 2) {
			char *p = buf + strlen(buf) - 3;
			if (strcmp("_-_", p) == 0) {
				sprintf(buf + strlen(buf) - 3, "%c", '-');
				++c;
			}
		}
		++i;
	}
	if (c > 0) {
		if (rename(s, buf)) {
			fprintf(stderr, "Filerename failed!\n");
			return 1;
		}
	} else {
		printf("Already OK");
	}
}
